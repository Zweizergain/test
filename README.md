## 低配跑团开放平台（Server）

# 第一迭代（已实现）
*  加载classpath下的剧本接口
*  创建、加入、离开房间接口
*  非归约操作的节点间转移接口

# 第二迭代
*  异常处理（规范化的代码格式和出参结构）
*  剧本上传、下载、删除接口
*  接口认证，比如发布固定密钥（剧本相关优先级高）
*  物品栏（也将为剧情flag共用）
