package com.zweizergain.scenario.entity;

import com.google.common.base.Strings;
import com.zweizergain.common.PlayerCommand;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScenarioTransfer {
    @NonNull
    private PlayerCommand command;
    /**
     * use regex (full match) to deal with various situations
     */
    private String pattern;

    /**
     * Check if given transfer is a subset of this transfer
     * @param other transfer that player input
     * @return result
     */
    public boolean check(@NonNull ScenarioTransfer other) {
        return other.command == command && (Strings.isNullOrEmpty(pattern) || Strings.nullToEmpty(other.pattern).matches(pattern));
    }
}
