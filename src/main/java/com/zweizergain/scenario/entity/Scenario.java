package com.zweizergain.scenario.entity;

import com.zweizergain.common.MyRuntimeException;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Optional;
import java.util.Set;

/**
 * If makers need a preload scene, add a preload node before normal root node
 * TODO support default player number
 */
@Data
@NoArgsConstructor
public class Scenario {
    /**
     * Initialize when loaded
     */
    @NonNull
    private String id;
    @NonNull
    private String name;
    @NonNull
    private Set<ScenarioNode> nodes;

    public ScenarioNode getRoot() {
        return nodes.stream().filter(ScenarioNode::checkRoot).findFirst().orElseThrow(() -> new MyRuntimeException(String.format("Scenario %s does not have a root node.", id)));
    }

    public Optional<ScenarioNode> getNodeByID(@NonNull String id) {
        return nodes.stream().filter(i -> id.equals(i.getId())).findFirst();
    }
}
