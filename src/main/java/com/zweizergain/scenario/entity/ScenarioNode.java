package com.zweizergain.scenario.entity;

import com.zweizergain.common.MyRuntimeException;
import lombok.*;

import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ScenarioNode {
    /**
     * "0" for root node
     */
    @NonNull
    private String id;
    @NonNull
    private String text;
    /**
     * ID of leaf node as key
     */
    private Map<String, ScenarioTransfer> transfers;

    public boolean checkRoot() {
        return "0".equals(id);
    }

    /**
     * Matching node should be only one
     * @param other transfer that player input
     * @return ID of target node
     */
    public String getTargetNodeID(@NonNull ScenarioTransfer other) {
        return transfers.entrySet().stream()
                .filter(t -> t.getValue().check(other))
                .findFirst()
                .orElseThrow(() -> new MyRuntimeException(String.format("None match for transfer %s.", other)))
                .getKey();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScenarioNode that = (ScenarioNode) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
