package com.zweizergain.scenario;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.zweizergain.scenario.entity.Scenario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class ScenarioContainer {
    /**
     * ID of scenario as key
     */
    private Map<String, Scenario> map = Maps.newConcurrentMap();

    private ObjectMapper objectMapper;

    @Autowired
    public ScenarioContainer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     *
     * @return number of current scenarios
     */
    public Mono<Long> load() {
        AtomicInteger integer = new AtomicInteger();
        return Mono.fromCallable(() -> new PathMatchingResourcePatternResolver().getResources("classpath:*.json"))
                .subscribeOn(Schedulers.elastic())
                .flatMapMany(Flux::fromArray)
                .map(r -> {
                    try {
                        return objectMapper.readValue(r.getFile(), Scenario.class);
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    return null;
                }).filter(Objects::nonNull)
                .doOnNext(s -> {
                    String key = String.valueOf(integer.getAndIncrement());
                    // Attention! Calling setters will affect upstreams
                    s.setId(key);
                    map.put(key, s);
                }).count();
    }

    public List<Scenario> getAll() {
        return ImmutableList.copyOf(map.values());
    }

    public Optional<Scenario> getByID(String id) {
        return Optional.ofNullable(map.get(id));
    }
}
