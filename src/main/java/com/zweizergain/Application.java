package com.zweizergain;

import com.zweizergain.common.PlayerCommand;
import com.zweizergain.scenario.entity.ScenarioNode;
import com.zweizergain.scenario.entity.ScenarioTransfer;
import com.zweizergain.web.entity.TourInput;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);

//        WebClient webClient = WebClient.create("http://localhost:8080");
//
//        System.out.println(webClient.get()
//                .uri("/scenario/load")
//                .retrieve()
//                .bodyToMono(Long.class)
//                .block());
//
//        TourInput input = new TourInput();
//        input.setTourID("tour1");
//        input.setScenarioID("0");
//        input.setUsername("lihao");
//        input.setTransfer(new ScenarioTransfer(PlayerCommand.NEXT, null));
//
//        System.out.println(webClient.post()
//                .uri("/tour/create")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .body(BodyInserters.fromObject(input))
//                .retrieve()
//                .bodyToMono(String.class)
//                .block());
//
//        System.out.println(webClient.post()
//                .uri("/tour/join")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .body(BodyInserters.fromObject(input))
//                .retrieve()
//                .bodyToMono(ScenarioNode.class)
//                .block());
//
//        System.out.println(webClient.post()
//                .uri("/tour/forward")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .body(BodyInserters.fromObject(input))
//                .retrieve()
//                .bodyToMono(ScenarioNode.class)
//                .block());
//
//        System.out.println(webClient.post()
//                .uri("/tour/leave")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .body(BodyInserters.fromObject(input))
//                .retrieve()
//                .bodyToMono(String.class)
//                .block());
    }
}
