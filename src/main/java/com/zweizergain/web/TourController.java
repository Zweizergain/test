package com.zweizergain.web;

import com.zweizergain.scenario.entity.ScenarioNode;
import com.zweizergain.tour.TourContainer;
import com.zweizergain.tour.entity.Tour;
import com.zweizergain.web.entity.TourInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("tour")
public class TourController {
    private TourContainer tourContainer;

    @Autowired
    public TourController(TourContainer tourContainer) {
        this.tourContainer = tourContainer;
    }

    @PostMapping("create")
    public Mono<String> create(@Validated(TourInput.Create.class) @RequestBody Mono<TourInput> input) {
        return tourContainer.create(input);
    }

    @PostMapping("join")
    public Mono<ScenarioNode> join(@Validated(TourInput.Join.class) @RequestBody Mono<TourInput> input) {
        return tourContainer.join(input);
    }

    @PostMapping("forward")
    public Mono<ScenarioNode> forward(@Validated(TourInput.Forward.class) @RequestBody Mono<TourInput> input) {
        return tourContainer.forward(input);
    }

    @PostMapping("leave")
    public Mono<String> leave(@Validated(TourInput.Leave.class) @RequestBody Mono<TourInput> input) {
        return tourContainer.leave(input);
    }

    /**
     * Just for testing now
     * @return tour map
     */
    @GetMapping("getAll")
    public Mono<Map<String, Tour>> getAll() {
        return Mono.just(tourContainer.getAll());
    }
}
