package com.zweizergain.web.entity;

import com.zweizergain.scenario.entity.ScenarioTransfer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TourInput {
    @NotBlank(groups = {Create.class, Join.class, Forward.class, Leave.class})
    private String tourID;
    @NotBlank(groups = Create.class)
    private String scenarioID;
    @NotBlank(groups = {Join.class, Forward.class, Leave.class})
    private String username;
    @NotNull(groups = Forward.class)
    private ScenarioTransfer transfer;

    public interface Create {}
    public interface Join {}
    public interface Forward {}
    public interface Leave {}
}
