package com.zweizergain.web;

import com.zweizergain.scenario.ScenarioContainer;
import com.zweizergain.scenario.entity.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * TODO security
 */
@RestController
@RequestMapping("scenario")
public class ScenarioController {
    private ScenarioContainer scenarioContainer;

    @Autowired
    public ScenarioController(ScenarioContainer scenarioContainer) {
        this.scenarioContainer = scenarioContainer;
    }

    @GetMapping("load")
    public Mono<Long> load() {
        return scenarioContainer.load();
    }

    /**
     * Just for testing now
     * @return list of scenarios
     */
    @GetMapping("getAll")
    public Flux<Scenario> getAll() {
        return Flux.fromIterable(scenarioContainer.getAll());
    }
}
