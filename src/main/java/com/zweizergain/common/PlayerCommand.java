package com.zweizergain.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum PlayerCommand {
    NEXT, VOTE(true), LEAVE;
    private boolean reduced = false;
}
