package com.zweizergain.tour;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.zweizergain.common.MyRuntimeException;
import com.zweizergain.scenario.ScenarioContainer;
import com.zweizergain.scenario.entity.ScenarioNode;
import com.zweizergain.tour.entity.Tour;
import com.zweizergain.web.entity.TourInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Optional;

@Component
public class TourContainer {
    /**
     * UUID as key, input by player now
     */
    private Map<String, Tour> map = Maps.newConcurrentMap();

    private ScenarioContainer scenarioContainer;

    @Autowired
    public TourContainer(ScenarioContainer scenarioContainer) {
        this.scenarioContainer = scenarioContainer;
    }

    /**
     * Do nothing if tourID already exists; unnecessary to be reactive now, but for persistence plan
     * @param input username is unnecessary now
     * @return message
     */
    public Mono<String> create(Mono<TourInput> input) {
        return input.doOnNext(i -> map.putIfAbsent(i.getTourID(), new Tour(scenarioContainer.getByID(i.getScenarioID()).orElseThrow(() -> new MyRuntimeException(String.format("ScenarioID %s does not exist.", i.getScenarioID()))))))
                .thenReturn("Done.")
                // temporary exception handling plan
                .onErrorResume(MyRuntimeException.class, e -> Mono.just(e.getMessage()));
    }

    public Mono<ScenarioNode> join(Mono<TourInput> input) {
        return input.map(i -> getByID(i.getTourID()).join(i.getUsername()));
    }

    public Mono<ScenarioNode> forward(Mono<TourInput> input) {
        // TODO handle reduced command in new method
        return input.map(i -> {
            Tour tour = getByID(i.getTourID());
            String scenarioID = tour.getCurrentNode(i.getUsername()).orElseThrow(() -> new MyRuntimeException(String.format("User %s does not in this tour.", i.getUsername()))).getTargetNodeID(i.getTransfer());
            ScenarioNode targetNode = tour.getScenario().getNodeByID(scenarioID).orElseThrow(() -> new MyRuntimeException(String.format("ScenarioID %s does not exist.", scenarioID)));
            tour.setCurrentNode(i.getUsername(), targetNode);
            return targetNode;
        });
    }

    /**
     * Remove the tour if last player has left
     * @param input tourID and username
     * @return message
     */
    public Mono<String> leave(Mono<TourInput> input) {
        return input.doOnNext(i -> {
            if (getByID(i.getTourID()).leave(i.getUsername()) == 0) {
                map.remove(i.getTourID());
            }
        }).thenReturn("Done.");
    }

    public Map<String, Tour> getAll() {
        return ImmutableMap.copyOf(map);
    }

    private Tour getByID(String id) {
        return Optional.ofNullable(map.get(id)).orElseThrow(() -> new MyRuntimeException(String.format("TourID %s does not exist.", id)));
    }
}
