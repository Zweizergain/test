package com.zweizergain.tour.entity;

import com.google.common.collect.Maps;
import com.zweizergain.scenario.entity.Scenario;
import com.zweizergain.scenario.entity.ScenarioNode;
import lombok.Data;
import lombok.NonNull;

import java.util.Map;
import java.util.Optional;

@Data
public class Tour {
    /**
     * Initialize when created, never change though scenario source file changed
     */
    @NonNull
    private Scenario scenario;
    /**
     * player name as key
     */
    private Map<String, ScenarioNode> currentNodeMap = Maps.newConcurrentMap();

    public Tour(Scenario scenario) {
        this.scenario = scenario;
    }

    public Optional<ScenarioNode> getCurrentNode(@NonNull String username) {
        return Optional.ofNullable(currentNodeMap.get(username));
    }

    public void setCurrentNode(@NonNull String username, @NonNull ScenarioNode targetNode) {
        currentNodeMap.put(username, targetNode);
    }

    public ScenarioNode join(@NonNull String username) {
        return currentNodeMap.computeIfAbsent(username, k -> scenario.getRoot());
    }

    /**
     *
     * @param username username to leave
     * @return current player number after left
     */
    public long leave(@NonNull String username) {
        currentNodeMap.remove(username);
        return currentNodeMap.size();
    }
}
